<?php
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$nombreProducto = $detalleProducto = $precioUnitario = $cantidadProducto = $proveedorProducto = "";
$nombreProducto_err = $detalleProducto_err = $precioUnitario_err = $cantidadProducto_err = $proveedorProducto_err = "";
 
// Processing form data when form is submitted
if(isset($_POST["idProducto"]) && !empty($_POST["idProducto"])){
    // Get hidden input value
    $idProducto = $_POST["idProducto"];
    
    // validar nombre del producto
    $input_nombreProdu = trim($_POST["nombreProducto"]);
    if(empty($input_nombreProdu)){
        $nombreProducto_err = "Please enter a name.";
    } else{
        $nombreProducto = $input_nombreProdu;
    }
    
    // validar detalle del producto
    $input_detalleProdu = trim($_POST["detalleProducto"]);
    if(empty($input_detalleProdu)){
        $nombreProducto_err = "Please enter an address.";     
    } else{
        $detalleProducto = $input_detalleProdu;
    }
    
    // validar precio unitario del producto
    $input_precioUni = trim($_POST["precioUnitario"]);
    if(empty($input_precioUni)){
        $precioUnitario_err = "Please enter the salary amount.";     
    } elseif(!ctype_digit($input_precioUni)){
        $precioUnitario_err = "Please enter a positive integer value.";
    } else{
        $precioUnitario = $input_precioUni;
    }


        // validar cantidad del producto
    $input_cantidadProdu = trim($_POST["cantidadProducto"]);
    if(empty($input_cantidadProdu)){
        $cantidadProducto_err = "Please enter the salary amount.";     
    } elseif(!ctype_digit($input_cantidadProdu)){
        $cantidadProducto_err = "Please enter a positive integer value.";
    } else{
        $cantidadProducto = $input_cantidadProdu;
    }

    // validar proveedor del producto
    $input_proveedorProdu = trim($_POST["proveedorProducto"]);
    if(empty($input_proveedorProdu)){
        $proveedorProducto_err = "Please enter an address.";     
    } else{
        $proveedorProducto = $input_proveedorProdu;
    }


    
    // Check input errors before inserting in database
    if(empty($name_err) && empty($address_err) && empty($salary_err)){
        // Prepare an update statement
        $sql = "UPDATE productos 
                SET nombreProducto=?, detalleProducto=?, precioUnitario=?, cantidadProducto=?, proveedorProducto=?
                WHERE idProducto=?";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sssssi", $param_nombreProdu, $param_detalleProdu, $param_precioUni, $param_cantidadProdu, $param_proveedorProd, $param_idProdu);
            
            // Set parameters
            $param_nombreProdu = $nombreProducto;
            $param_detalleProdu = $detalleProducto;
            $param_precioUni = $precioUnitario;
            $param_cantidadProdu = $cantidadProducto;
            $param_proveedorProd = $proveedorProducto;
            $param_idProdu = $idProducto;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records updated successfully. Redirect to landing page
                header("location: indexInventario.php");
                exit();
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["idProducto"]) && !empty(trim($_GET["idProducto"]))){
        // Get URL parameter
        $idProducto =  trim($_GET["idProducto"]);
        
        // Prepare a select statement
        $sql = "SELECT * FROM productos WHERE idProducto = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_idProdu);
            
            // Set parameters
            $param_idProdu = $idProducto;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
    
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $nombreProducto = $row["nombreProducto"];
                    $detalleProducto = $row["detalleProducto"];
                    $precioUnitario = $row["precioUnitario"];
                    $cantidadProducto = $row["cantidadProducto"];
                    $proveedorProducto = $row["proveedorProducto"];

                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    header("location: error.php");
                    exit();
                }
                
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
        
        // Close connection
        mysqli_close($link);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mt-5">Actualizar producto</h2>
                    <p>Please edit the input values and submit to update the employee record.</p>
                    <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                    <div class="form-group">
                            <label>Nombre del producto</label>
                            <input type="text" name="nombreProducto" class="form-control <?php echo (!empty($nombreProducto_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $nombreProducto; ?>">
                            <span class="invalid-feedback"><?php echo $nombreProducto_err;?></span>
                        </div>
                        <div class="form-group">
                            <label>Detalle del producto</label>
                            <textarea name="detalleProducto" class="form-control <?php echo (!empty($detalleProducto_err)) ? 'is-invalid' : ''; ?>"><?php echo $detalleProducto; ?></textarea>
                            <span class="invalid-feedback"><?php echo $detalleProducto_err;?></span>
                        </div>
                        <div class="form-group">
                            <label>Precio unitario</label>
                            <input type="text" name="precioUnitario" class="form-control <?php echo (!empty($precioUnitario_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $precioUnitario; ?>">
                            <span class="invalid-feedback"><?php echo $precioUnitario_err;?></span>
                        </div>
                        <div class="form-group">
                            <label>Cantidad del producto</label>
                            <input type="text" name="cantidadProducto" class="form-control <?php echo (!empty($cantidadProducto_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $cantidadProducto; ?>">
                            <span class="invalid-feedback"><?php echo $cantidadProducto_err;?></span>
                        </div>
                        <div class="form-group">
                            <label>Proveedor del producto</label>
                            <input type="text" name="proveedorProducto" class="form-control <?php echo (!empty($proveedorProducto_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $proveedorProducto; ?>">
                            <span class="invalid-feedback"><?php echo $proveedorProducto_err;?></span>
                        </div>
                        <input type="hidden" name="idProducto" value="<?php echo $idProducto; ?>"/>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="indexInventario.php" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>