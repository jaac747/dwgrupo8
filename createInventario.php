<?php

require_once "config.php";
 
// Define variables and initialize with empty values
$nombreProducto = $detalleProducto = $precioUnitario = $cantidadProducto = $proveedorProducto = "";
$nombreProducto_err = $detalleProducto_err = $precioUnitario_err = $cantidadProducto_err = $proveedorProducto_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // validar nombre del producto
    $input_nombreProdu = trim($_POST["nombreProducto"]);
    if(empty($input_nombreProdu)){
        $nombreProducto_err = "Please enter a name.";
    } else{
        $nombreProducto = $input_nombreProdu;
    }
    
    // validar detalle del producto
    $input_detalleProdu = trim($_POST["detalleProducto"]);
    if(empty($input_detalleProdu)){
        $nombreProducto_err = "Please enter an address.";     
    } else{
        $detalleProducto = $input_detalleProdu;
    }
    
    // validar precio unitario del producto
    $input_precioUni = trim($_POST["precioUnitario"]);
    if(empty($input_precioUni)){
        $precioUnitario_err = "Please enter the salary amount.";     
    } elseif(!ctype_digit($input_precioUni)){
        $precioUnitario_err = "Please enter a positive integer value.";
    } else{
        $precioUnitario = $input_precioUni;
    }


        // validar cantidad del producto
    $input_cantidadProdu = trim($_POST["cantidadProducto"]);
    if(empty($input_cantidadProdu)){
        $cantidadProducto_err = "Please enter the salary amount.";     
    } elseif(!ctype_digit($input_cantidadProdu)){
        $cantidadProducto_err = "Please enter a positive integer value.";
    } else{
        $cantidadProducto = $input_cantidadProdu;
    }

    // validar proveedor del producto
    $input_proveedorProdu = trim($_POST["proveedorProducto"]);
    if(empty($input_proveedorProdu)){
        $proveedorProducto_err = "Please enter an address.";     
    } else{
        $proveedorProducto = $input_proveedorProdu;
    }




    
    // Check input errors before inserting in database
    if(empty($nombreProducto_err) && empty($detalleProducto_err) && empty($precioUnitario_err) && empty($cantidadProducto_err) && empty($proveedorProducto_err)){
        // Prepare an insert statement
        $sql = "INSERT INTO productos (nombreProducto, detalleProducto, precioUnitario, cantidadProducto, proveedorProducto) VALUES (?, ?, ?, ?, ?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sssss", $param_nombreProdu, $param_detalleProdu, $param_precioUni, $param_cantidadProdu, $param_proveedorProd);
            
            // Set parameters
            $param_nombreProdu = $nombreProducto;
            $param_detalleProdu = $detalleProducto;
            $param_precioUni = $precioUnitario;
            $param_cantidadProdu = $cantidadProducto;
            $param_proveedorProd = $proveedorProducto;
  
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records created successfully. Redirect to landing page
                header("location: indexInventario.php");
                exit();
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mt-5">Crear Productos</h2>
                    <p>Please fill this form and submit to add employee record to the database.</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group">
                            <label>Nombre del producto</label>
                            <input type="text" name="nombreProducto" class="form-control <?php echo (!empty($nombreProducto_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $nombreProducto; ?>">
                            <span class="invalid-feedback"><?php echo $nombreProducto_err;?></span>
                        </div>
                        <div class="form-group">
                            <label>Detalle del producto</label>
                            <textarea name="detalleProducto" class="form-control <?php echo (!empty($detalleProducto_err)) ? 'is-invalid' : ''; ?>"><?php echo $detalleProducto; ?></textarea>
                            <span class="invalid-feedback"><?php echo $detalleProducto_err;?></span>
                        </div>
                        <div class="form-group">
                            <label>Precio unitario</label>
                            <input type="text" name="precioUnitario" class="form-control <?php echo (!empty($precioUnitario_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $precioUnitario; ?>">
                            <span class="invalid-feedback"><?php echo $precioUnitario_err;?></span>
                        </div>
                        <div class="form-group">
                            <label>Cantidad del producto</label>
                            <input type="text" name="cantidadProducto" class="form-control <?php echo (!empty($cantidadProducto_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $cantidadProducto; ?>">
                            <span class="invalid-feedback"><?php echo $cantidadProducto_err;?></span>
                        </div>
                        <div class="form-group">
                            <label>Proveedor del producto</label>
                            <input type="text" name="proveedorProducto" class="form-control <?php echo (!empty($proveedorProducto_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $proveedorProducto; ?>">
                            <span class="invalid-feedback"><?php echo $proveedorProducto_err;?></span>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="indexInventario.php" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>