<?php
// Check existence of id parameter before processing further
if(isset($_GET["idProducto"]) && !empty(trim($_GET["idProducto"]))){
    // Include config file
    require_once "config.php";
    
    // Prepare a select statement
    $sql = "SELECT * FROM productos WHERE idProducto = ?";
    
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "i", $param_idProdu);
        
        // Set parameters
        $param_idProdu = trim($_GET["idProducto"]);
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);
    
            if(mysqli_num_rows($result) == 1){
                /* Fetch result row as an associative array. Since the result set
                contains only one row, we don't need to use while loop */
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                
                // Retrieve individual field value
                $nombreProducto = $row["nombreProducto"];
                $detalleProducto = $row["detalleProducto"];
                $precioUnitario = $row["precioUnitario"];
                $cantidadProducto = $row["cantidadProducto"];
                $proveedorProducto = $row["proveedorProducto"];
            } else{
                // URL doesn't contain valid id parameter. Redirect to error page
                header("location: error.php");
                exit();
            }
            
        } else{
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
     
    // Close statement
    mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
} else{
    // URL doesn't contain id parameter. Redirect to error page
    header("location: error.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        .wrapper{
            width: 600px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="mt-5 mb-3">View Record</h1>
                    <div class="form-group">
                        <label>Nombre del producto</label>
                        <p><b><?php echo $row["nombreProducto"]; ?></b></p>
                    </div>
                    <div class="form-group">
                        <label>Detalle del producto</label>
                        <p><b><?php echo $row["detalleProducto"]; ?></b></p>
                    </div>
                    <div class="form-group">
                        <label>Precio por unidad</label>
                        <p><b><?php echo $row["precioUnitario"]; ?></b></p>
                    </div>
                    <div class="form-group">
                        <label>Cantidad del producto</label>
                        <p><b><?php echo $row["cantidadProducto"]; ?></b></p>
                    </div>
                    <div class="form-group">
                        <label>Proveedor del producto</label>
                        <p><b><?php echo $row["proveedorProducto"]; ?></b></p>
                    </div>
                    <p><a href="indexInventario.php" class="btn btn-primary">Back</a></p>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>