
const PRODUCTO_URL = 'http://localhost/backendfinal/api/prodVenta.php';
const VENTA_URL = 'http://localhost/backendfinal/api/venta.php';
const IMAGENES_URL = 'http://localhost/backendfinal/imag/';


var productosVentas = [];
function obtenerUsuarios() {
    axios({
        method: 'GET',
        url: PRODUCTO_URL,
        responseType: 'json'
    }).then(res => {
        console.log(res.data);
        this.productosVentas = res.data;
        llenarPlantilla();
    }).catch(error => {
        console.error(error);
    });
}

obtenerUsuarios();

function llenarPlantilla(){
    document.querySelector('#productos').innerHTML ='';
    for (let i = 0; i < productosVentas.length; i++) {
        if ( productosVentas[i].cantidadProducto>0 ) {
            document.querySelector('#productos').innerHTML +=
                `<div class="product_sale">
                    <input type="hidden" name="disponible-${i}" id="disponible-${i}" value="${productosVentas[i].cantidadProducto}">
                    <div class="search-product-image">
                        <img src="${IMAGENES_URL}${productosVentas[i].nombreProducto}.jpg" >
                    </div>
                    <div class="serach-product-info">
                        <label class="text-capitalize">${productosVentas[i].nombreProducto}</label>
                        <label>Q ${productosVentas[i].precioUnitario}</label>
                        <p><label class="producto-disponible">Producto disponible:${productosVentas[i].cantidadProducto}</label></p>
                    </div>
                    <div class="caption">
                        <label for="cantidad-${i}">Cantidad:</label><input type="number" name="cantidad-${i}" id="cantidad-${i}" class="cantidad-producto">
                        <button class="btn btn-success btn-sm pull-right" onclick="agregar(${productosVentas[i].idProducto},${i})">Agregar</button>
                    </div>
                </div>`;
        }else {
            document.querySelector('#productos').innerHTML +=
                `<div class="product_sale">
                    <input type="hidden" name="disponible-${i}" id="disponible-${i}" value="0">
                    <div class="search-product-image">
                        <img src="${IMAGENES_URL}${productosVentas[i].nombreProducto}.jpg" >
                    </div>
                    <div class="serach-product-info">
                        <label class="text-capitalize">${productosVentas[i].nombreProducto}</label>
                        <label>Q ${productosVentas[i].precioUnitario}</label>
                        <p><label class="producto-no-disponible">Producto no disponible</label></p>
                    </div>

                    <div class="caption">
                        <label for="cantidad-${i}">Cantidad:</label><input type="number" name="cantidad-${i}" id="cantidad-${i}" class="cantidad-producto" disabled>
                        <button class="btn btn-success btn-sm pull-right" onclick="agregar(${productosVentas[i].idProducto},${i})" disabled>Agregar</button>
                    </div>
                </div>`;
        }
    }
}
var carritoVentas= [];
var countCarritoVentas = 0;
function agregar(idProducto,idElemento){

    var cantidad = document.getElementById("cantidad-"+idElemento).value;
    var disponible = +document.getElementById("disponible-"+idElemento).value;
    if(isEmpty(cantidad)){
        alert("No puede dejar la cantidad vacia");
        return;
    }else if(cantidad<1){
        alert("La cantidad debe ser mayor a cero");
        return;
    } 
    console.log(cantidad);
    console.log(disponible);
    if(cantidad >disponible){
        alert("No hay suficiente Stock disponible para este producto");
        return;
    }
    carritoVentas[countCarritoVentas] = {"idProducto":idProducto, "cantidad":cantidad};
    countCarritoVentas++;
    console.log(carritoVentas);
    document.getElementById("Canasta").textContent=countCarritoVentas+"";
    document.getElementById("cantidad-"+idElemento).value="";
    
}

function isEmpty(str) {
    return (!str || str.length === 0 );
}

function pagar(){
    if(countCarritoVentas<1){
        alert("No ha agregado productos al carrito");
        return;
    }
    var jsonBody = {
        "idUsuario":1,
        "pedido":carritoVentas
    };
    console.log(jsonBody);
    
    axios({
        method: 'POST',
        url: VENTA_URL,
        responseType: 'json',
        data:jsonBody
    }).then(res => {
        console.log(res);
        result = res.data;
        alert("Pedido realizado Exitosamente");
        document.getElementById("Canasta").textContent="";
        countCarritoVentas = 0;
        obtenerUsuarios();
    }).catch(error => {
        console.error(error);
    });

}
