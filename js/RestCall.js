     function getApi(urlstr) {
        var result = [];
        axios({
            method: 'GET',
            url: urlstr,
            responseType: 'json'
        }).then(res => {
            console.log(res.data);
            result = res.data;
        }).catch(error => {
            console.error(error);
        });
        return result;
    }

     function addApi(urlstr,jsonBody) {
        var result = [];
        axios({
            method: 'POST',
            url: urlstr,
            responseType: 'json',
            params:jsonBody
        }).then(res => {
            console.log(res.data);
            result = res.data;
        }).catch(error => {
            console.error(error);
        });
        return result;
    }

     function updateApi(urlstr,jsonBody){
        var result = [];
        axios({
            method: 'UPDATE',
            url: urlstr,
            responseType: 'json',
            params:jsonBody
        }).then(res => {
            console.log(res.data);
            result = res.data;
        }).catch(error => {
            console.error(error);
        });
        return result;
    }

     function deleteApi(urlstr, id){
        var result = false;
        axios({
            method: 'DELETE',
            url: urlstr + `?id=${id}`,
            responseType: 'json'
        }).then(res => {
            console.log(res.data);
            result = true;
        }).catch(error => {
            console.error(error);
            result = false;
        });
        return result;
    }
