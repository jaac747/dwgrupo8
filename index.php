<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GRUPO 8 - DESARROLLO WEB</title>
    <link rel="stylesheet" href="./css/style.css">
    <!-- <script type="text/javascript" src="./js/babel.js"></script> -->

</head>

<body>
    <div class="page">
        <header tabindex="0">GRUPO 8 - DESARROLLO WEB</header>
        <div id="nav-container">
            <div class="bg"></div>
            <div class="button" tabindex="0">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </div>
            <div id="nav-content" tabindex="0">
                <ul>
                    <li><a href="#0">Gestión de usuarios</a></li>
                    <li><a href="productoVentas.php">Realizar pedidos</a></li>
                    <li><a href="#0">Seguimientos de pedidos</a></li>
                    <li><a href="indexInventario.php">Administración de inventarios</a></li>
                    <li><a href="#0">Contact</a></li>
                    <li class="small"><a href="#0">Facebook</a><a href="#0">Instagram</a></li>
                </ul>
            </div>
        </div>



        <div class="container">
            <div class="">
                <h2>Off-screen navigation using <span>:focus-within</span></h2>
                <p>Adding yet another pure CSS technique to the list of off-screen navigation by "hacking" the :focus-within pseudo-class. Have a look at the code to see how it works.</p>
                <small><strong>NB!</strong> Use a browser that supports :focus-within</small>
            </div>
        </div>
    </div>


</body>

</html>