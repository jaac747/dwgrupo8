
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/styleMenu.css">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" >
    <title>Document</title>
</head>
<body>
    <header tabindex="0">
        <h2 class="titulo">GRUPO 8 - DESARROLLO WEB</h2>
        <div class="menu_bar">
            <a href="#" class="bt-menu"><span class="icon-menu"></span>Menu</a>
        </div>
        <nav>
            <ul>
                <li><a href="index.php"><span class="icon-home"></span>Inicio</a></li>
                <li><a href="#0"><span class="icon-users"></span>Gestión de usuarios</a></li>
                <li><a href="productoVentas.php"><span class="icon-cart"></span>Realizar pedidos</a></li>
                <li><a href="#0"><span class="icon-truck"></span>Estado pedidos</a></li>
                <li><a href="indexInventario.php"><span class="icon-briefcase"></span>Inventarios</a></li>
                
            </ul>
        </nav>
    </header>